# Arcade Server
A simple application to retrieve and distribute games data
## Perequisites
- JWT Endpoint Authentication
- JSON Schema Validation
- Server logs
- MongoDB Integration
- Firebase Integration

## Modules
1. User Authentication
   - User registration
   - User login
   - Get user data
2. User Profile
   - Update user preferences
     - Edit user preferred genres
     - Edit user preferred platforms
   - Update user wishlist
3. Game Giveaways
   - Current game giveaways
     - View giveaway information
   - DLC giveaways
   - Beta game access
4. Recent Game Deals
   - Top rated game deals
   - Filter game deals
   - View deal information
5. Browse Games
   - Search games by user preferences
   - View game information
   - Add/Remove game from wishlist
  
# Powered by
- [Gamer Power](https://www.gamerpower.com/api-read)
- [Cheap Shark](https://apidocs.cheapshark.com/)
- [RAWG](https://rawg.io/apidocs)
