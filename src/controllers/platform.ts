import {Response} from 'express';
import {AuthRequest} from "../interfaces/AuthRequest";
import User from "../schema/User.schema";
import {ServerResponse} from "../interfaces/ServerResponse";
import Platform from "../schema/Platform.schema";

export const updatePlatforms = async (req: AuthRequest, res: Response) => {
    const uid = req.uid;
    const platforms = req.body;

    await User.findByIdAndUpdate(uid, {preferredPlatforms: platforms}, {
        new: true,
        runValidators: true
    });

    const updated: ServerResponse = {
        success: true,
        message: 'Platforms updated successfully'
    }

    res.status(201).json(updated);
}

export const allPlatforms = async (req: AuthRequest, res: Response) => {
    const genres = await Platform.find({}, '_id name slug').exec();

    const genreResponse: ServerResponse = {
        success: true,
        message: 'All platforms retrieved successfully',
        payload: genres
    }

    res.status(200).json(genreResponse);
}
