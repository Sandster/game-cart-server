import {Request, Response} from 'express'
import axios from "axios";
import {ServerResponse} from "../interfaces/ServerResponse";
import dotenv from 'dotenv';
import {parse} from "url";

dotenv.config();

const url = `${process.env.CHEAP_SHARK_BASE_URL}deals?`;

//most recent deals
export const recentDeals = async (req: Request, resp: Response) => {
    const {pageSize, pageNumber, min: minimumPrice, max: maximumPrice, sale: onSale} = req.query;

    let query = `${url}pageSize=${pageSize}&pageNumber=${pageNumber}`;

    if (minimumPrice) {
        query += `&lowerPrice=${minimumPrice}`;
    }

    if (maximumPrice) {
        query += `&upperPrice=${maximumPrice}`;
    }

    if (onSale) {
        query += `&onSale=${onSale === 'true'}`;
    }

    const dealsPayload = await axios.get(query);

    const deals: ServerResponse = {
        results: dealsPayload.data.length,
        page: parseInt(pageNumber as string),
        success: true,
        message: 'Deals retrieved successfully',
        payload: dealsPayload.data
    }

    resp.status(200).json(deals);
};

//deal lookup
export const dealLookup = async (req: Request, resp: Response) => {
    const {id: dealID} = req.body

    const lookupPayload = await axios.get(`${url}id=${dealID}`);
    const data: ServerResponse = {
        success: true,
        message: 'Deals information retrieved successfully',
        payload: [lookupPayload.data.gameInfo],
        stores: lookupPayload.data.cheaperStores,
        cheapestPrice: lookupPayload.data.cheapestPrice
    }
    resp.status(200).json(data);
};
