import {Response} from 'express';
import {AuthRequest} from "../interfaces/AuthRequest";
import Wishlist from '../schema/Wishlist.schema';
import {ServerResponse} from "../interfaces/ServerResponse";
import {ApiError} from "../errors/ApiError";

export const addToWishlist = async (req: AuthRequest, resp: Response) => {
    const uid = req.uid;
    const wishlistData = req.body;

    //check if already exists
    const {wishlist} = await Wishlist.findById(uid).exec();
    const available = wishlist.some((game: any) => game._id === wishlistData._id);

    if (available) {
        throw new ApiError(`Game already present in wishlist`, 409);
    }

    //add to wishlist
    await Wishlist.findByIdAndUpdate(uid,
        {
            $push: {wishlist: wishlistData}
        }, {
            new: true,
            runValidators: true
        });

    const data: ServerResponse = {
        success: true,
        message: 'Added to wishlist'
    }

    resp.status(200).json(data);
}

export const getWishlist = async (req: AuthRequest, res: Response) => {
    const uid = req.uid;
    const {wishlist} = await Wishlist.findById(uid).exec();
    const data: ServerResponse = {
        success: true,
        message: 'Wishlist retrieved successfully',
        payload: wishlist
    }

    res.status(200).json(data);
}

export const checkGameAvailability = async (req: AuthRequest, res: Response) => {
    const uid = req.uid;
    const {id: gameID} = req.query;

    const parent = await Wishlist.findById(uid).exec();
    const selectedGame = parent.wishlist.id(gameID);

    const data: ServerResponse = {
        success: true,
        message: 'Game not available in wishlist'
    }
    if (selectedGame != null) {
        data.message = 'Game present in wishlist'
        data.present = true;
    } else {
        data.present = false;
    }
    res.status(200).json(data);
}

export const removeFromWishList = async (req: AuthRequest, res: Response) => {
    const uid = req.uid;
    const {id: gameID} = req.query;

    const parent = await Wishlist.findById(uid).exec();
    const selectedGame = parent.wishlist.id(gameID);

    if (selectedGame === null) {
        throw new ApiError(`Game doesn't exist in the wishlist`, 404);
    } else {
        selectedGame.remove();
    }
    await parent.save();

    const data: ServerResponse = {
        success: true,
        message: 'Game removed from wishlist'
    }
    res.status(200).json(data);
}
