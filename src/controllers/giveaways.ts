import {Request, Response} from 'express';
import axios from "axios";
import {ServerResponse} from "../interfaces/ServerResponse";
import dotenv from "dotenv";
import {ApiError} from "../errors/ApiError";

dotenv.config();

const url = `${process.env.GAMER_POWER_BASE_URL}giveaway`;

export const liveGiveaways = async (req: Request, resp: Response) => {
    const {type, platform} = req.query;

    let query = `${url}s?type=${type}`
    if (platform) {
        query += `&platform=${platform}`;
    }
    try {
        const giveawayPayload = await axios.get(query);

        const page = Number(req.query.page) || 1;
        const limit = Number(req.query.limit) || 10;

        const giveawayData = giveawayPayload.data;
        const skip = (page - 1) * limit;

        const project = giveawayData.slice(skip, skip + limit);

        const giveaways: ServerResponse = {
            results: project.length,
            success: true,
            message: 'Giveaways retrieved successfully',
            payload: project
        }
        resp.status(200).json(giveaways);
    } catch (error: any) {
        if (error.response.status === 404) {
            throw new ApiError(`No results found, check parameters and try again`, 400);
        }
    }
}

export const giveawayLookup = async (req: Request, res: Response) => {
    const {id: gameID} = req.query;

    const query = `${url}?id=${gameID}`

    try {
        const giveawayPayload = await axios.get(query);
        const giveawayInfo: ServerResponse = {
            success: true,
            message: 'Giveaway info retrieved successfully',
            payload: [giveawayPayload.data]
        }
        res.status(200).json(giveawayInfo);
    } catch (error: any) {
        if (error.response.status === 404) {
            throw new ApiError(`No results found with the given ID`, 400);
        }
    }
}
