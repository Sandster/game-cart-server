import {Request, Response} from 'express'
import dotenv from "dotenv";
import axios from "axios";
import {ServerResponse} from "../interfaces/ServerResponse";
import {ApiError} from "../errors/ApiError";
import {IGame} from "../schema/Wishlist.schema";

dotenv.config();

export const searchGame = async (req: Request, res: Response) => {
    const {gameSlug: slug, id} = req.query;

    if (slug === undefined && id === undefined) {
        throw new ApiError('Game ID or game slug is required', 400);
    } else if (slug !== undefined && id !== undefined) {
        throw new ApiError('Please select from one search type, either by name or id', 400);
    }

    let searchQuery: string | number = '';
    if (slug) {
        searchQuery = slug as string;
    }

    if (id) {
        searchQuery = parseInt(id as string);
    }

    try {
        const url = `${process.env.RAWG_BASE_URL}games/${searchQuery}?key=${process.env.RAWG_API_KEY}`;
        const searchResult = await axios.get(url);

        const payload = searchResult.data;

        const filteredProperties = generateGameObject(payload, true);

        const game: ServerResponse = {
            success: true,
            message: 'Game retrieved successfully',
            payload: [filteredProperties]
        }

        res.status(200).json(game);
    } catch (error: any) {
        if (error.response.status === 404)
            throw new ApiError('No results found', 404);
    }
}

export const gamesList = async (req: Request, res: Response) => {
    const {search, resultCount} = req.query;

    const searchQuery = search as string;
    const url = `${process.env.RAWG_BASE_URL}games?search=${searchQuery.toLowerCase()}&key=${process.env.RAWG_API_KEY}`;
    const allGames = await axios.get(url);

    let searchResults = allGames.data.results;
    if (resultCount) {
        searchResults = allGames.data.results.slice(0, parseInt(resultCount as string));
    }

    const games = searchResults.map((gameData: any) => {
        return generateGameObject(gameData, false);
    });

    const result: ServerResponse = {
        results: games.length,
        success: true,
        message: 'Game search successful',
        payload: games
    }

    res.status(200).json(result);
}

export const gamesByGenres = async (req: Request, res: Response) => {
    const {genres: preferredGenres, resultCount} = req.query;

    let url = `${process.env.RAWG_BASE_URL}games?key=${process.env.RAWG_API_KEY}`;

    if (preferredGenres) {
        url += `&genres=${preferredGenres}`;
    }

    const defaultGames = await axios.get(url);
    let searchResults = defaultGames.data.results;
    if (resultCount) {
        searchResults = defaultGames.data.results.slice(0, parseInt(resultCount as string));
    }

    const games = searchResults.map((gameData: any) => {
        return generateGameObject(gameData, true);
    });

    const result: ServerResponse = {
        results: games.length,
        success: true,
        message: 'Games retrieved by genres',
        payload: games
    }
    res.status(200).json(result);

}

const generateGameObject = (gameData: any, showStores: boolean): IGame => {
    const platforms = gameData.parent_platforms.map((item: any) => {
        const {id: _id, name, slug} = item.platform;
        return {_id: _id.toString(), name, slug}
    });

    const genres = gameData.genres.map(({id: _id, name, slug}: any) => {
        return {_id:_id.toString(), name, slug}
    });

    let description: string = ''

    if (gameData.description !== undefined) {
        description = gameData.description.replace(/(<([^>]+)>)/ig, '');
    }

    let stores: [] = [];

    if (showStores) {
        stores = gameData.stores.map((storeItem: any) => {
            return {
                id: storeItem.store.id,
                name: storeItem.store.name,
                domain: storeItem.store.domain,
            }
        });
    }

    const game: IGame = {
        _id: gameData.id,
        name: gameData.name,
        slug: gameData.slug,
        description,
        platforms,
        releaseDate: gameData.released,
        backgroundImage: gameData.background_image,
        rating: gameData.rating,
        metacritic: gameData.metacritic !== null ? gameData.metacritic : 0,
        esrb: gameData.esrb_rating !== null ? gameData.esrb_rating.name : 'Not Available',
        genres,
        stores
    }

    return game;
}
