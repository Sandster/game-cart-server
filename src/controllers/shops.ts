import {Request, Response} from 'express'
import dotenv from "dotenv";
import axios from "axios";
import {ServerResponse} from "../interfaces/ServerResponse";

dotenv.config();

const url = `${process.env.CHEAP_SHARK_BASE_URL}stores`;
const imageUrl = process.env.CHEAP_SHARK_IMAGE_BASE_URL;

export const getStore = async (req: Request, res: Response) => {
    const {id: storeId} = req.query;

    const storePayload = await axios.get(url);

    const stores = storePayload.data;

    const filtered = stores.filter((store: any) => {
        return store.storeID === storeId && store.isActive === 1;
    }).map((store: any) => {
        return {
            id: store.storeID,
            name: store.storeName,
            image: imageUrl + store.images.logo
        }
    });

    const storeAvailability: boolean = filtered.length > 0;

    const storeData: ServerResponse = {
        success: storeAvailability,
        message: storeAvailability ? 'Store data retrieved successfully' : 'Store data unavailable',
        payload: filtered
    }

    res.status(200).json(storeData);
}
