import {Request, Response} from 'express'
import {createUserWithEmailAndPassword, getAuth, signInWithEmailAndPassword} from "firebase/auth";
import admin from "firebase-admin";
import User, {IUser} from "../schema/User.schema";
import Wishlist from "../schema/Wishlist.schema";
import {getLogger} from "log4js";
import {Claims} from "../interfaces/Claims";
import serviceAccount from '../service-account.json';
import {ApiError} from "../errors/ApiError";
import {AuthRequest} from "../interfaces/AuthRequest";
import {ServerResponse} from "../interfaces/ServerResponse";

admin.initializeApp(
    {
        credential: admin.credential.cert(serviceAccount as admin.ServiceAccount)
    }
);

export const registerUser = async (req: Request, resp: Response) => {
    const claims: Claims = {
        registered: true,
        verified: false,
    };
    getLogger().info("Attempting to register user");
    const {username, email, password} = req.body;
    const auth = getAuth();
    try {
        //create user
        const createUser = await createUserWithEmailAndPassword(auth, email, password);
        const {uid} = await createUser.user;
        const accessToken = await createUser.user.getIdToken(false);
        getLogger().info(`${email} initial registration complete`);

        //set custom claims
        await admin.auth().setCustomUserClaims(uid, claims);
        getLogger().info(`Claims added`);

        const userData: IUser = {
            _id: uid,
            username: username,
            email: email,
            preferredGenres: [],
            preferredPlatforms: []
        }

        //save user data
        const newUser = new User(userData);
        await newUser.save();
        getLogger().info(`${email} registration successful`);

        //create an empty wishlist
        const wishlist = new Wishlist({_id: uid, wishlist: []});
        await wishlist.save();
        getLogger().info(`Wishlist created for ${email}`);

        const registerResponse: ServerResponse = {
            success: true,
            message: `Registered successfully`,
            token: accessToken
        }
        resp.status(201).json(registerResponse);
    } catch (error: any) {
        if (error.code === 'auth/email-already-in-use') {
            throw new ApiError(`Email already registered`, 200);
        }
    }
};

export const loginUser = async (req: Request, resp: Response) => {
    const registerData = req.body;
    const auth = getAuth()
    try {
        const userCredential = await signInWithEmailAndPassword(auth, registerData.email, registerData.password);
        const email = userCredential.user.email;

        const accessKey = await userCredential.user.getIdToken(false);

        getLogger().info(`User logged in ${email}`);

        getLogger().info('User verified, attempting to generate tokens');

        const loginResponse: ServerResponse = {
            success: true,
            message: 'Login successful',
            token: accessKey
        }

        resp.status(200).json(loginResponse);

    } catch (error: any) {
        getLogger().error(error);
        if (error.code === 'auth/wrong-password' || error.code === 'auth/user-not-found') {
            throw new ApiError('Invalid email or password', 200);
        }
    }
};

export const getUser = async (req: AuthRequest, res: Response) => {
    const uid = req.uid;

    const userData = await User.findById(uid, 'username email preferredGenres preferredPlatforms').exec();

    const user: ServerResponse = {
        success: true,
        message: 'User retrieved successfully',
        payload: [userData]
    }

    res.status(200).json(user);
}
