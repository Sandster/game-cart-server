import {Response} from 'express'
import {AuthRequest} from "../interfaces/AuthRequest";
import User from "../schema/User.schema";
import {ServerResponse} from "../interfaces/ServerResponse";
import Genre from "../schema/Genre.schema";

export const updateUserGenres = async (req: AuthRequest, res: Response) => {
    const uid = req.uid;
    const genres = req.body;

    await User.findByIdAndUpdate(uid, {preferredGenres: genres}, {
        new: true,
        runValidators: true
    });

    const updated: ServerResponse = {
        success: true,
        message: 'Genres updated successfully'
    }

    res.status(201).json(updated);
}

export const getAllGenres = async (req: AuthRequest, res: Response) => {

    const genres = await Genre.find({}, '_id name slug').exec();

    const genreResponse: ServerResponse = {
        success: true,
        message: 'All genres retrieved successfully',
        payload: genres
    }

    res.status(200).json(genreResponse);
}

