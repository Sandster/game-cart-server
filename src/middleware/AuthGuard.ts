import {Response, NextFunction} from 'express';
import {getLogger} from 'log4js';
import dotenv from 'dotenv';
import {AuthRequest} from "../interfaces/AuthRequest";
import {ServerResponse} from "../interfaces/ServerResponse";
import admin from "firebase-admin";

dotenv.config();
const logger = getLogger("server");

export const authGuard = (req: AuthRequest, resp: Response, next: NextFunction) => {
    const bearerHeader = req.headers['authorization'];

    const validity: ServerResponse = {
        success: false,
        message: '',
    }

    if (bearerHeader !== undefined) {
        const bearer = bearerHeader.split(' ');
        const token = bearer[1];

        //check whether the token is active
        admin.auth().verifyIdToken(token).then((decodedToken) => {
            const uid = decodedToken.uid;
            logger.info('JWT verified');
            req.uid = uid;
            next();
        }).catch((error: any) => {
            logger.error(error);
            if (error.errorInfo.code === 'auth/argument-error'){
                validity.message = 'Invalid token';
                validity.code = 'auth/invalid';
            }else if (error.errorInfo.code === 'auth/id-token-expired' || error.errorInfo.code === 'auth/id-token-revoked'){
                validity.message = 'Token expired or revoked';
                validity.code = 'auth/expired';
            }else{
                validity.message = 'Authorization failed, please try again';
                validity.code = 'auth/failed';
            }
            resp.status(401).json(validity);
        });
    } else {
        logger.error(`Authorization key not found`);
        validity.message = `Authorization key not found`;
        resp.status(401).json(validity);
    }
};
