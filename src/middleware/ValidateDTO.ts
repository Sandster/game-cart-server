import {Request, Response, NextFunction} from 'express';
import {getLogger} from "log4js";

export const validateDTO = (ajvValidate: any, queryParam: boolean) => {
    return (req: Request, res: Response, next: NextFunction) => {
        let valid = ajvValidate(req.body);
        if (queryParam) {
            valid = ajvValidate(req.query);
        }
        if (!valid) {
            const errors = ajvValidate.errors;
            getLogger().error(errors);
            res.status(400).json(errors);
        } else {
            next();
        }
    }
}
