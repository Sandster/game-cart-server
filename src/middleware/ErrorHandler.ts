import {Request, Response, NextFunction} from 'express'
import {ServerResponse} from "../interfaces/ServerResponse";
import {ApiError} from "../errors/ApiError";
import {getLogger} from "log4js";

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {

    const error: ServerResponse = {
        success: false,
        message: 'Something went wrong, please try again later',
        payload: [
            err
        ]
    }
    if (err instanceof ApiError) {
        error.message = err.message;
        delete error.payload
        return res.status(err.statusCode).json(error);
    }
    getLogger().error(err);
    return res.status(500).json({error});
}
