export class ApiError extends Error {

    statusCode: number;

    constructor(message: string, statusCode: number) {
        super(message);
        this.statusCode = statusCode
    }
}

export const createApiError = (message: string, statusCode: number): ApiError => {
    return new ApiError(message, statusCode);
}

