export interface ResponseKeys {
    accessToken: string,
    refreshToken: string
}
