export interface ServerResponse {
    results?: number,
    message: string | null,
    success: boolean,
    payload?: any [],

    [key: string]: any
}
