export interface GenrePlatformProps {
    _id: string,
    name: string,
    slug: string,
}
