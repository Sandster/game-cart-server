import Ajv from 'ajv';
import addFormats from 'ajv-formats'


const ajvInstance = new Ajv({allErrors: true});
require('ajv-errors')(ajvInstance)
addFormats(ajvInstance);

export default ajvInstance;
