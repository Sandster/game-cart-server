import express from 'express';
import {initializeApp} from 'firebase/app';
import {configure, getLogger} from 'log4js';
import helmet from 'helmet';
import cors from 'cors';
import rateLimit from 'express-rate-limit';

const xss = require('xss-clean')

require('express-async-errors');

import dealsRoute from './routes/api/deals';
import authenticationRoute from './routes/api/authentication';
import wishlistRoute from './routes/api/wishlist';
import giveawayRoute from './routes/api/giveaways';
import storeRoute from './routes/api/shops';
import browseRoute from './routes/api/browse';
import genreRoute from './routes/api/genre';
import platformRoute from './routes/api/platform';

import {connectDB} from './db';
import dotenv from 'dotenv';
import {notFound} from "./middleware/NotFound";
import {errorHandler} from "./middleware/ErrorHandler";

dotenv.config();

const PORT = process.env.PORT || 5000;
const app = express();

configure({
    appenders: {server: {type: "file", filename: "server.log"}},
    categories: {default: {appenders: ["server"], level: "debug"}}
});

const firebaseConfig = {
    apiKey: "AIzaSyCKVAPdjt8q8UGoGCtcEuWEvPxQGqN82FA",
    authDomain: "game-cart-cd45a.firebaseapp.com",
    projectId: "game-cart-cd45a",
    storageBucket: "game-cart-cd45a.appspot.com",
    messagingSenderId: "527289670485",
    appId: "1:527289670485:web:fef5895bb44cba006deddc"
};

initializeApp(firebaseConfig);

app.set('trust proxy', 1);
app.use(rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 100,
    standardHeaders: true
}));
app.use(express.json());
app.use(helmet());
app.use(cors());
app.use(xss());

app.use("/api/deals", dealsRoute);
app.use("/api/auth", authenticationRoute);
app.use("/api/wishlist", wishlistRoute);
app.use("/api/browse", browseRoute);
app.use("/api/genre", genreRoute);
app.use("/api/platform", platformRoute);
app.use("/api/giveaways", giveawayRoute);
app.use("/api/stores", storeRoute);

//URL validation
app.use(notFound);
app.use(errorHandler);

const start = async () => {
    try {
        const conn = await connectDB();
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
        getLogger().info(`Database connected at ${conn.connection.host}`)
    } catch (error) {
        getLogger().error(error);
        process.exit(1);
    }
}

start();
