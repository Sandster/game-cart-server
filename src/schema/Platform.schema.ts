import {GenrePlatformProps} from "../interfaces/GenrePlatformProps";
import mongoose, {Schema} from "mongoose";

export const PlatformSchema = new Schema<GenrePlatformProps>(
    {
        _id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        slug: {
            type: String,
            required: true,
        }
    }
);

export default mongoose.model('Platform', PlatformSchema);
