import mongoose, {Schema} from "mongoose";
import {GenrePlatformProps} from "../interfaces/GenrePlatformProps";

export const GenreItemSchema = new Schema<GenrePlatformProps>(
    {
        _id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        slug: {
            type: String,
            required: true,
        }
    }
);

export default mongoose.model('Genre', GenreItemSchema);
