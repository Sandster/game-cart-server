import {GenrePlatformProps} from "../interfaces/GenrePlatformProps";
import {PlatformSchema} from "./Platform.schema";
import {GenreItemSchema} from "./Genre.schema";
import mongoose, {Schema} from "mongoose";

export interface IGame {
    _id: number,
    name: string,
    slug?: string,
    description: string | undefined,
    platforms: GenrePlatformProps[],
    releaseDate?: string,
    backgroundImage?: string,
    rating: number,
    metacritic?: number,
    esrb: string,
    genres: GenrePlatformProps[],
    addedAt?: Date
    stores?: any []
}

const wishlistItemSchema = new Schema<IGame>(
    {
        _id: {
            type: Number,
            required: true,
            unique: true
        },
        name: {
            type: String,
            required: true
        },
        description: {
            type: String
        },
        slug: {
            type: String,
            required: true,
        },
        platforms: {
            type: [PlatformSchema],
            required: true
        },
        releaseDate: {
            type: String,
            required: true
        },
        backgroundImage: String,
        rating: {
            type: Number,
            required: true
        },
        metacritic: Number,
        esrb: {
            type: String,
            required: true
        },
        genres: {
            type: [GenreItemSchema],
            required: true
        },
        addedAt: {
            type: Date,
            default: Date.now()
        }
    });

const wishlistSchema = new Schema(
    {
        _id: {
            type: String,
            required: true
        },
        wishlist: {
            type: [wishlistItemSchema],
            required: true,
            unique: true
        }
    }
);

export default mongoose.model('Wishlist', wishlistSchema);



