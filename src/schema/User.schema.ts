import mongoose, {Schema} from "mongoose";
import GenreItemSchema from "./Genre.schema";
import {GenrePlatformProps} from "../interfaces/GenrePlatformProps";

export interface IUser {
    _id: string,
    username: string,
    email: string,
    preferredGenres: any[],
    preferredPlatforms: string[],
    avatar?: string,
    createdAt?:Date
}

const UserSchema = new Schema<IUser>(
    {
        _id: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
        },
        preferredGenres: [],
        preferredPlatforms: [],
        avatar: String,
        createdAt: {
            type: Date,
            default: Date.now()
        }
    }
)

export default mongoose.model('User', UserSchema);
