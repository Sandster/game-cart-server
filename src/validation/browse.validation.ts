import ajvInstance from "../ajv-instance";

const gameListSchema = {
    type: 'object',
    properties: {
        search: {type: 'string'},
        resultCount: {type: 'string'}
    },
    required: ['search', 'resultCount'],
    additionalProperties: false
}

const searchGame = {
    type: 'object',
    properties: {
        gameSlug: {type: 'string'},
        id: {type: 'string'}
    },
    anyOf: [
        {
            required: ['gameSlug']
        },
        {
            required: ['id']
        }
    ],
    additionalProperties: false
}

const genreGameSearch = {
    type: 'object',
    properties: {
        genres: {
            type: 'string',
            pattern: '^[a-z0-9]+((,|-)[a-z0-9]+)*[a-z0-9]+$'
        },
        resultCount: {type: 'string'}
    },
    required: ['resultCount'],
    additionalProperties: false,
    errorMessage: {
        properties: {
            genres: 'should be a string separated by a comma'
        }
    }
}

export const gameListSchemaValidation = ajvInstance.compile(gameListSchema);
export const gameSearchSchemaValidation = ajvInstance.compile(searchGame);
export const genreGameSearchValidation = ajvInstance.compile(genreGameSearch);
