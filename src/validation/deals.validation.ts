import ajvInstance from "../ajv-instance";


const saleSchema = {
    type: 'object',
    properties: {
        pageSize: {type: 'string'},
        pageNumber: {type: 'string'},
        min: {type: 'string'},
        max: {type: 'string'},
        sale: {type: 'string'}
    },
    required: ['pageSize', 'pageNumber'],
    additionalProperties: false
}

const lookupSchema = {
    type: 'object',
    properties: {
        id: {type: 'string'}
    },
    required: ['id'],
    additionalProperties: false
}
export const saleSchemaValidation = ajvInstance.compile(saleSchema);
export const dealLookupSchemaValidation = ajvInstance.compile(lookupSchema);
