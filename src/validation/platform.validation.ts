import ajvInstance from "../ajv-instance";
import {genreSchema} from "./genre.validation";

const updatePlatformSchema = {
    type: 'array',
    items: genreSchema
}

//genre schema has the same structure as platform

export const platformUpdateSchemaValidation = ajvInstance.compile(updatePlatformSchema);
