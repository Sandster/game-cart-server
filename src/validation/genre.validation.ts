import ajvInstance from "../ajv-instance";

export const genreSchema = {
    type: 'object',
    properties: {
        _id: {type: 'string'},
        name: {type: 'string'},
        slug: {type: 'string'},
    },
    required: ['_id', 'name', 'slug'],
    additionalProperties: false
}

const updateGenreSchema = {
    type: 'array',
    items: genreSchema
}

export const genreUpdateSchemaValidation = ajvInstance.compile(updateGenreSchema);
