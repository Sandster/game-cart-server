import ajvInstance from "../ajv-instance";

const liveGiveawaySchema = {
    type: 'object',
    properties: {
        platform: {enum: ['pc', 'playstation', 'xbox', 'ios', 'android', 'mac', 'linux', 'nintendo']},
        type: {enum: ['game', 'beta', 'loot']},
        page: {type: 'string'},
        limit: {type: 'string'}
    },
    required: ['type'],
    additionalProperties: false
}

const giveawayLookupSchema = {
    type: 'object',
    properties: {
        id: {type: 'string'}
    },
    required: ['id'],
    additionalProperties: false
}
export const liveGiveawayValidationSchema = ajvInstance.compile(liveGiveawaySchema);
export const giveawayLookupSchemaValidation = ajvInstance.compile(giveawayLookupSchema);
