import ajvInstance from "../ajv-instance";


const loginSchema = {
    type: 'object',
    properties: {
        email: {type: 'string', format: 'email'},
        password: {type: 'string', format: 'password'}
    },
    required: ['email', 'password'],
    additionalProperties: false
}

const registrationSchema = {
    type: 'object',
    properties: {
        username: {type: 'string'},
        email: {type: 'string', format: 'email'},
        password: {type: 'string', format: 'password'}
    },
    required: ['username', 'email', 'password'],
    additionalProperties: false
}

export const loginSchemaValidation = ajvInstance.compile(loginSchema);
export const registrationSchemaValidation = ajvInstance.compile(registrationSchema);
