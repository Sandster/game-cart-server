import ajvInstance from "../ajv-instance";

const genrePlatformSchema = {
    type: 'object',
    properties: {
        _id: {type: 'string'},
        name: {type: 'string'},
        slug: {type: 'string'}
    },
    required: ['_id', 'name', 'slug'],
    additionalProperties: false
}

const addWishListItemSchema = {
    type: 'object',
    properties: {
        name: {type: 'string'},
        slug: {type: 'string'},
        platforms: {
            type: 'array',
            uniqueItems: true,
            items: genrePlatformSchema
        },
        releaseDate: {type: 'string'},
        backgroundImage: {type: 'string'},
        rating: {type: 'number'},
        metacritic: {type: 'number'},
        _id: {type: 'number'},
        esrb: {type: 'string'},
        genres: {
            type: 'array',
            uniqueItems: true,
            items: genrePlatformSchema
        }
    },
    required: ['name', 'platforms', 'releaseDate', 'rating', 'metacritic', '_id', 'esrb', 'genres'],
    additionalProperties: false
}

const removeGameSchema = {
    type: 'object',
    properties: {
        id: {type: 'string'}
    },
    required: ['id'],
    additionalProperties: false
}

export const addWishListItemSchemaValidation = ajvInstance.compile(addWishListItemSchema);
export const removeGameSchemaValidation = ajvInstance.compile(removeGameSchema);
