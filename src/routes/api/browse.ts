import express from "express";
import {authGuard} from "../../middleware/AuthGuard";
import {searchGame, gamesList, gamesByGenres} from "../../controllers/browse";
import {
    gameListSchemaValidation,
    gameSearchSchemaValidation,
    genreGameSearchValidation
} from "../../validation/browse.validation";
import {validateDTO} from "../../middleware/ValidateDTO";

const route = express.Router();

route.route('/').get([authGuard, validateDTO(gameListSchemaValidation, true)], gamesList);
route.route('/get').get([authGuard, validateDTO(gameSearchSchemaValidation, true)], searchGame);
route.route('/genre').get([authGuard, validateDTO(genreGameSearchValidation, true)], gamesByGenres);

export default route;
