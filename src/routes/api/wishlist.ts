import {validateDTO} from "../../middleware/ValidateDTO";
import {addWishListItemSchemaValidation, removeGameSchemaValidation} from "../../validation/wishlist.validation";
import express from "express";
import {authGuard} from "../../middleware/AuthGuard";
import {addToWishlist, getWishlist, removeFromWishList, checkGameAvailability} from "../../controllers/wishlist";

const route = express.Router();

route.route('/').get(authGuard, getWishlist);
route.route('/add').post([validateDTO(addWishListItemSchemaValidation, false), authGuard], addToWishlist);
route.route('/remove').delete([authGuard, validateDTO(removeGameSchemaValidation, true)], removeFromWishList);
route.route('/check').get([authGuard, validateDTO(removeGameSchemaValidation, true)], checkGameAvailability);

export default route;
