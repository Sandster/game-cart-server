import express from 'express';
import {registerUser, loginUser, getUser} from '../../controllers/authentication'
import {validateDTO} from "../../middleware/ValidateDTO";
import {loginSchemaValidation, registrationSchemaValidation} from "../../validation/authentication.validation";
import {authGuard} from "../../middleware/AuthGuard";

const router = express.Router();

router.route('/get').get(authGuard, getUser);
router.route('/register').post(validateDTO(registrationSchemaValidation, false), registerUser);
router.route('/login').post(validateDTO(loginSchemaValidation, false), loginUser);

export default router;


