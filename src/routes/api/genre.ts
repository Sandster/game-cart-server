import express from "express";
import {updateUserGenres, getAllGenres} from "../../controllers/genre";
import {authGuard} from "../../middleware/AuthGuard";
import {genreUpdateSchemaValidation} from "../../validation/genre.validation";
import {validateDTO} from "../../middleware/ValidateDTO";

const route = express.Router();

route.route('/').get(authGuard, getAllGenres);
route.route('/update').patch([authGuard, validateDTO(genreUpdateSchemaValidation, false)], updateUserGenres);

export default route;
