import express from "express";
import {validateDTO} from "../../middleware/ValidateDTO";
import {liveGiveaways, giveawayLookup} from "../../controllers/giveaways";
import {authGuard} from "../../middleware/AuthGuard";
import {liveGiveawayValidationSchema, giveawayLookupSchemaValidation} from "../../validation/giveaway.validation";

const route = express.Router();

route.route('/').get([authGuard, validateDTO(liveGiveawayValidationSchema, true)], liveGiveaways);
route.route('/lookup').get([authGuard, validateDTO(giveawayLookupSchemaValidation, true)], giveawayLookup);

export default route;
