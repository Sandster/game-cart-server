import express from "express";
import {authGuard} from "../../middleware/AuthGuard";
import {getStore} from "../../controllers/shops";

const route = express.Router();

route.route('/lookup').get([authGuard], getStore);

export default route;
