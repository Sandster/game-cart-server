import {validateDTO} from "../../middleware/ValidateDTO";
import express from "express";
import {dealLookupSchemaValidation, saleSchemaValidation} from "../../validation/deals.validation";
import {dealLookup, recentDeals} from "../../controllers/deals";
import {authGuard} from "../../middleware/AuthGuard";

const route = express.Router();

route.route('/onsale').get([authGuard, validateDTO(saleSchemaValidation, true)], recentDeals);
route.route('/lookup').post([authGuard, validateDTO(dealLookupSchemaValidation, false)], dealLookup);

export default route;
