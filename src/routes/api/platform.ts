import express from "express";
import {allPlatforms, updatePlatforms} from "../../controllers/platform";
import {authGuard} from "../../middleware/AuthGuard";
import {validateDTO} from "../../middleware/ValidateDTO";
import {platformUpdateSchemaValidation} from "../../validation/platform.validation";

const route = express.Router();

route.route('/').get(authGuard, allPlatforms);
route.route('/update').patch([authGuard, validateDTO(platformUpdateSchemaValidation, false)], updatePlatforms);

export default route;
